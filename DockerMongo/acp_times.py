"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow



#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


# Minimum times as [(from_dist, to_dist, speed),
#                   (from_dist, to_dist, speed), ... ]
min_speed = [(0, 200, 15), (200, 400, 15), (400, 600, 15),
             (600, 1000, 11.428), (1000, 1300, 13.333)]
max_speed = [(0, 200, 34), (200, 400, 32), (400, 600, 30),
             (600, 1000, 28), (1000, 1300, 28)]

# Final control times (at or exceeding brevet distance) are special cases
final_close = {200: 13.5, 300: 20, 400: 27, 600: 40, 1000: 75}
max_dist = 1300

def open_time(controlDist, brevetDist, brevetStartTime):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    brevetTime = arrow.get(brevetStartTime, 'YYYY-MM-DD HH:mm')
    """
    elapsed_hours = 0
    distance_left = controlDist
    for from_dist, to_dist, speed in max_speed:
        seg_length = to_dist - from_dist
        if distance_left > seg_length:
            elapsed_hours += seg_length / speed
            distance_left -= seg_length
        else:
            elapsed_hours += distance_left / speed
            dayTime = int(elapsed_hours/24)
            hourTime, minuteTime = divmod(elapsed_hours, 1)
            print(hourTime, minuteTime)
            minuteTime = round(minuteTime*60)
            openTime = brevetTime.replace(day=dayTime,hour=int(hourTime), minute=int(minuteTime))
            openTime = openTime.isoformat()
            openTime = openTime[:-6]
            return openTime
    """    
    #Used a while loop to make sure the code exits if it gets a value.
    hourValue = -1
    while hourValue == -1:
        if controlDist < 200:
            hourValue = controlDist/34
            break
        elif controlDist <= 400:
            hourValue = (controlDist-200)/32 + 200/34
            break
        elif controlDist <= 600:
            hourValue = (controlDist-400)/30 + 200/32 + 200/34
            break
        elif controlDist <= 1000:
            hourValue = (controlDist-600)/28 + 200/30 + 200/32 + 200/34
            break
        elif controlDist <= 1300:
            hourValue = (controlDist-1000)/26 + 200/28 + 200/30 + 200/32 + 200/34
            break

    hourTime, minuteTime = divmod(hourValue, 1)
    #Round to solid minutes before converting back for hourValue
    minuteTime = round(minuteTime*60)
    print(hourTime, minuteTime)
    brevetTime = brevetTime.shift(hours=+hourTime, minutes=+minuteTime)
    brevetTime = brevetTime.isoformat()
    brevetTime = brevetTime[:-6]
    return brevetTime



def close_time(controlDist, brevetDist, brevetStartTime):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    brevetTime = arrow.get(brevetStartTime, 'YYYY-MM-DD HH:mm')
    
    #Used a while loop to make sure the code exits if it gets a value.
    hourValue = -1
    while hourValue == -1:
        if controlDist == 0:
            hourValue = 1
            break

        #French variation
        elif controlDist <= 60:
            hourValue = controlDist/20 + 1
            break     
        else:
            if controlDist == 200:
                hourValue = 13.5
                break
            elif controlDist == 300:
                hourValue = 20
                break
            elif controlDist == 400:
                hourValue = 27
                break
            elif controlDist == 600:
                hourValue = 40
                break
            elif controlDist == 1000:
                hourValue = 75
                break
            else:
                if controlDist <= 600:
                    hourValue = controlDist/15
                    break
                elif controlDist <= 1000:
                    hourValue = (controlDist-600)/11.428 + 600/15
                    break
                elif controlDist <= 1300:
                    hourValue = (controlDist-1000)/13.333 + 600/15 + 1000/11.428
                    break
        
    hourTime, minuteTime = divmod(hourValue, 1)
    #Round to solid minutes before converting back for hourValue
    minuteTime = round(minuteTime*60)
    print(hourTime, minuteTime)
    brevetTime = brevetTime.shift(hours=+hourTime, minutes=+minuteTime)
    brevetTime = brevetTime.isoformat()
    brevetTime = brevetTime[:-6]
    return brevetTime
