#Times Service
import os
import csv
from flask import Flask, request
from flask_cors import CORS
from flask_restful import Resource, Api

from pymongo import MongoClient

'''
This code is a clusterfuck of spaghetti but it is functional. 

Refactor for final project for the love of god.
'''
# Instantiate the app
app = Flask(__name__)
CORS(app)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevets

def returnDataBaseInformation():
	try:
		openList = db.brevets.find_one({'tag':'openTimes'})['timesList']
		closeList = db.brevets.find_one({'tag':'closeTimes'})['timesList']

		result = {"openTimes": openList, "closeTimes": closeList}
		return result
	except TypeError:
		errorValue = "ERROR. No data received! Did you send any? You have to send in some closing times first!"
		result = {"openTimes": errorValue, "closeTimes": errorValue}
		return result


#When you want the first [firstValue] entries.
def returnTopValues(firstValue, result):
	openList = result['openTimes']
	closeList = result['closeTimes']
	
	newOpen = []
	newClose = []
	
	maxItems = 0
	for i in range(0, len(openList)):
		if maxItems < firstValue and len(openList[i]) != 0:
			newOpen.append(openList[i])
			newClose.append(closeList[i])
			maxItems += 1


	firstReturnValues = {"openTimes": newOpen, "closeTimes":newClose }

	return firstReturnValues

class OpenAndClose(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue == None:		
			return result
		else:
			return returnTopValues(firstValue, result)

class OpenAndCloseJSON(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue == None:		
			return result
		else:
			return returnTopValues(firstValue, result)

class OpenAndCloseCSV(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue != None:		
			newResult = returnTopValues(firstValue, result)
			result = newResult

		openCSV = ','.join(result['openTimes'])
		closeCSV = ','.join(result['closeTimes'])
		
		finalCSV = "OpenTimes=" + openCSV + " CloseTimes=" + closeCSV	
		
		return finalCSV

class OpenTimes(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue == None:		
			return result['openTimes']
		else:
			return returnTopValues(firstValue, result)['openTimes']
class OpenTimesJSON(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue == None:		
			return result['openTimes']
		else:
			return returnTopValues(firstValue, result)['openTimes']

class OpenTimesCSV(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue != None:		
			newResult = returnTopValues(firstValue, result)
			result = newResult

		openCSV = ','.join(result['openTimes'])
		closeCSV = ','.join(result['closeTimes'])
		
		return openCSV	
		

class CloseTimes(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue == None:		
			return result['closeTimes']
		else:
			return returnTopValues(firstValue, result)['closeTimes']

class CloseTimesJSON(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue == None:		
			return result['closeTimes']
		else:
			return returnTopValues(firstValue, result)['closeTimes']


class CloseTimesCSV(Resource):
	def get(self):
		result = returnDataBaseInformation()
		firstValue = request.args.get('top', default=None, type=int)
		if firstValue != None:		
			newResult = returnTopValues(firstValue, result)
			result = newResult

		openCSV = ','.join(result['openTimes'])
		closeCSV = ','.join(result['closeTimes'])
		
		return closeCSV	
		
# Create routes
# Another way, without decorators
api.add_resource(OpenAndClose, '/listAll')
api.add_resource(OpenAndCloseJSON, '/listAll/json')
api.add_resource(OpenAndCloseCSV, '/listAll/csv')

api.add_resource(OpenTimes, '/listOpenOnly')
api.add_resource(OpenTimesJSON, '/listOpenOnly/json')
api.add_resource(OpenTimesCSV, '/listOpenOnly/csv')
api.add_resource(CloseTimes,'/listCloseOnly')
api.add_resource(CloseTimesJSON,'/listCloseOnly/json')
api.add_resource(CloseTimesCSV,'/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5001, debug=True)
