# Project 6: Brevet time calculator API

## Author: Shahriar Rahman
## Contact:shahriar@uoregon.edu
## 2020-05-22##

### Description of Calculator
ACP sanctioned brevet calculator that calculates the control point timing based on distances given. It works for values up to 1000km and calculates the time based on the rusa table of minimum and maximum speeds allowed. The calculator works in increments such that the first 200km of a control point will use a certain speed to calculate time and so on for the next 200km. It uses the french variation for start times such that for distances less than 60km, the closing time will follow the french rules. 

### Description of API and everything

Main Program is on port 5000.
API is on port 5001
Consumer program is on port 5002



When using the main program, Sending Data will send information with the cell location while retrieving it will update the cells. If the cells are not changed before retrieving the old data, the website will essentially look the same.

Consumer program will return the data into a string format for both the CSV and JSON. You can view the JSON specifically on port 5001/listAll and 5001/listAll/json

Changing the text field with a number or input will return the top k of that version
